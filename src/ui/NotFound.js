const NotFound = () => {
    return (
        <div>
            Ooops! Não encontramos essa página!
        </div>
    )
}

export default NotFound