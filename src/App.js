import { BrowserRouter, Link } from "react-router-dom"
import AppRouter from "./ui/AppRouter"

const App = () => {

    return (
        <BrowserRouter>
            <h1>Exemplo React Navigation</h1>
            <Link to="/">Início</Link> &nbsp;&nbsp;
            <Link to="/feature-a?source=menu">Feature A</Link> &nbsp;&nbsp;
            <Link to="/feature-b">Feature B</Link> &nbsp;&nbsp;
            <Link to="/feature-c/123">Feature C</Link> 

            <hr />

            <AppRouter />

        </BrowserRouter>
    )

}

export default App
