import React from "react"
import { withRouter } from "react-router-dom"

class FeatureB extends React.Component {

    constructor(props) {
        super(props)
        this.history = props.history
    }

    goToFeatureC = () => {
        this.history.push('/feature-c')
    }

    render() {
        return (
            <div>
                Bem-vindo à Feature B! <br /> <br />
                <input type="button" value="Ir para feature c" onClick={this.goToFeatureC}/>
            </div>
        )    
    }
}

export default withRouter(FeatureB)