import { Route, Switch } from "react-router-dom"
import FeatureA from "./FeatureA"
import FeatureB from "./FeatureB"
import FeatureC from "./FeatureC"
import Home from "./Home"
import NotFound from "./NotFound"

const AppRouter = () => {

    return (
        <Switch>
            <Route path="/" exact>
                <Home />
            </Route>
            <Route path="/feature-a">
                <FeatureA />
            </Route>
            <Route path="/feature-b">
                <FeatureB />
            </Route>
            <Route path="/feature-c/:paramId?">
                <FeatureC />
            </Route>
            <Route path="*">
                <NotFound />
            </Route>
        </Switch>
    )
}

export default AppRouter