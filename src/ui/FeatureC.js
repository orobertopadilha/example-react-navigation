import { useParams } from "react-router-dom"

const FeatureC = () => {
    const { paramId } = useParams()

    return (
        <div>
            Bem-vindo à Feature C!

            <br />
            Parâmetro recebido: {paramId}

        </div>
    )
}

export default FeatureC