import { useHistory, useLocation } from "react-router-dom"

const FeatureA = () => {
    const history = useHistory()
    const query = new URLSearchParams(useLocation().search)

    const goFeatureB = () => {
        history.push('/feature-b')
    }

    return (
        <div>
            Bem-vindo à Feature A! <br /><br />
            
            Parâmetro source = {query.get('source')}
            <br />
            <input type="button" value="Ir para feature b" onClick={goFeatureB} />
        </div>
    )
}

export default FeatureA